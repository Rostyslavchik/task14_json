
package com.rostyslavprotsiv.model.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * This is a visual elements of each knife element
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "blade",
    "handle",
    "bloodGroove"
})
public class Visual {

    /**
     * This is a blade of each knife element
     * (Required)
     * 
     */
    @JsonProperty("blade")
    @JsonPropertyDescription("This is a blade of each knife element")
    private Blade blade;
    /**
     * This is a handle of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handle")
    @JsonPropertyDescription("This is a handle of each knife element")
    private Handle handle;
    /**
     * This is a blood groove presence of each knife element
     * (Required)
     * 
     */
    @JsonProperty("bloodGroove")
    @JsonPropertyDescription("This is a blood groove presence of each knife element")
    private Boolean bloodGroove;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * This is a blade of each knife element
     * (Required)
     * 
     */
    @JsonProperty("blade")
    public Blade getBlade() {
        return blade;
    }

    /**
     * This is a blade of each knife element
     * (Required)
     * 
     */
    @JsonProperty("blade")
    public void setBlade(Blade blade) {
        this.blade = blade;
    }

    /**
     * This is a handle of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handle")
    public Handle getHandle() {
        return handle;
    }

    /**
     * This is a handle of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handle")
    public void setHandle(Handle handle) {
        this.handle = handle;
    }

    /**
     * This is a blood groove presence of each knife element
     * (Required)
     * 
     */
    @JsonProperty("bloodGroove")
    public Boolean getBloodGroove() {
        return bloodGroove;
    }

    /**
     * This is a blood groove presence of each knife element
     * (Required)
     * 
     */
    @JsonProperty("bloodGroove")
    public void setBloodGroove(Boolean bloodGroove) {
        this.bloodGroove = bloodGroove;
    }

    @Override
    public String toString() {
        return "Visual{" +
                "blade=" + blade +
                ", handle=" + handle +
                ", bloodGroove=" + bloodGroove + '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(blade).append(handle).append(bloodGroove).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Visual) == false) {
            return false;
        }
        Visual rhs = ((Visual) other);
        return new EqualsBuilder().append(blade, rhs.blade).append(handle, rhs.handle).append(bloodGroove, rhs.bloodGroove).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
