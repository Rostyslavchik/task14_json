
package com.rostyslavprotsiv.model.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * Cold steel
 * <p>
 * Here are unique options of cold steel created by human kind ever!
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "knifes"
})
public class ColdSteelSchema {

    /**
     * This is a root element of JSON file
     * (Required)
     * 
     */
    @JsonProperty("knifes")
    @JsonPropertyDescription("This is a root element of JSON file")
    private List<Knife> knifes = new ArrayList<Knife>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * This is a root element of JSON file
     * (Required)
     * 
     */
    @JsonProperty("knifes")
    public List<Knife> getKnifes() {
        return knifes;
    }

    /**
     * This is a root element of JSON file
     * (Required)
     * 
     */
    @JsonProperty("knifes")
    public void setKnifes(List<Knife> knifes) {
        this.knifes = knifes;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("\n");
        knifes.stream().forEach(s -> builder.append(s).append("\n"));
        return "ColdSteelSchema{ knifes=" + builder + '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(knifes).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ColdSteelSchema) == false) {
            return false;
        }
        ColdSteelSchema rhs = ((ColdSteelSchema) other);
        return new EqualsBuilder().append(knifes, rhs.knifes).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
