
package com.rostyslavprotsiv.model.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "handy",
    "origin",
    "visual",
    "value"
})
public class Knife {

    /**
     * This is a type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("This is a type of each knife element")
    private Type type;
    /**
     * This is a handy type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handy")
    @JsonPropertyDescription("This is a handy type of each knife element")
    private Handy handy;
    /**
     * This is a origin country of each knife element
     * (Required)
     * 
     */
    @JsonProperty("origin")
    @JsonPropertyDescription("This is a origin country of each knife element")
    private String origin;
    /**
     * This is a visual elements of each knife element
     * (Required)
     * 
     */
    @JsonProperty("visual")
    @JsonPropertyDescription("This is a visual elements of each knife element")
    private Visual visual;
    /**
     * This is a evaluation element of each knife element
     * (Required)
     * 
     */
    @JsonProperty("value")
    @JsonPropertyDescription("This is a evaluation element of each knife element")
    private Value value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * This is a type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("type")
    public Type getType() {
        return type;
    }

    /**
     * This is a type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * This is a handy type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handy")
    public Handy getHandy() {
        return handy;
    }

    /**
     * This is a handy type of each knife element
     * (Required)
     * 
     */
    @JsonProperty("handy")
    public void setHandy(Handy handy) {
        this.handy = handy;
    }

    /**
     * This is a origin country of each knife element
     * (Required)
     * 
     */
    @JsonProperty("origin")
    public String getOrigin() {
        return origin;
    }

    /**
     * This is a origin country of each knife element
     * (Required)
     * 
     */
    @JsonProperty("origin")
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * This is a visual elements of each knife element
     * (Required)
     * 
     */
    @JsonProperty("visual")
    public Visual getVisual() {
        return visual;
    }

    /**
     * This is a visual elements of each knife element
     * (Required)
     * 
     */
    @JsonProperty("visual")
    public void setVisual(Visual visual) {
        this.visual = visual;
    }

    /**
     * This is a evaluation element of each knife element
     * (Required)
     * 
     */
    @JsonProperty("value")
    public Value getValue() {
        return value;
    }

    /**
     * This is a evaluation element of each knife element
     * (Required)
     * 
     */
    @JsonProperty("value")
    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Knife{" +
                "type=" + type +
                ", handy=" + handy +
                ", origin='" + origin + '\'' +
                ", visual=" + visual +
                ", value=" + value +
                '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(handy).append(origin).append(visual).append(value).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Knife) == false) {
            return false;
        }
        Knife rhs = ((Knife) other);
        return new EqualsBuilder().append(type, rhs.type).append(handy, rhs.handy).append(origin, rhs.origin).append(visual, rhs.visual).append(value, rhs.value).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

    public enum Handy {

        ONE_ARM("one-arm"),
        TWO_ARMS("two-arms");
        private final String value;
        private final static Map<String, Handy> CONSTANTS = new HashMap<String, Handy>();

        static {
            for (Handy c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Handy(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Handy fromValue(String value) {
            Handy constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum Type {

        KNIFE("knife"),
        SWORD("sword"),
        AXE("axe"),
        DAGGER("dagger");
        private final String value;
        private final static Map<String, Type> CONSTANTS = new HashMap<String, Type>();

        static {
            for (Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Type fromValue(String value) {
            Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
