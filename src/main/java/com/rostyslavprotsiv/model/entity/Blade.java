
package com.rostyslavprotsiv.model.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * This is a blade of each knife element
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "length",
    "width",
    "material"
})
public class Blade {

    /**
     * This is a length of each knife element
     * (Required)
     * 
     */
    @JsonProperty("length")
    @JsonPropertyDescription("This is a length of each knife element")
    private String length;
    /**
     * This is a width of each knife element
     * (Required)
     * 
     */
    @JsonProperty("width")
    @JsonPropertyDescription("This is a width of each knife element")
    private String width;
    /**
     * This is a blade material of each knife element
     * (Required)
     * 
     */
    @JsonProperty("material")
    @JsonPropertyDescription("This is a blade material of each knife element")
    private Material material;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * This is a length of each knife element
     * (Required)
     * 
     */
    @JsonProperty("length")
    public String getLength() {
        return length;
    }

    /**
     * This is a length of each knife element
     * (Required)
     * 
     */
    @JsonProperty("length")
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * This is a width of each knife element
     * (Required)
     * 
     */
    @JsonProperty("width")
    public String getWidth() {
        return width;
    }

    /**
     * This is a width of each knife element
     * (Required)
     * 
     */
    @JsonProperty("width")
    public void setWidth(String width) {
        this.width = width;
    }

    /**
     * This is a blade material of each knife element
     * (Required)
     * 
     */
    @JsonProperty("material")
    public Material getMaterial() {
        return material;
    }

    /**
     * This is a blade material of each knife element
     * (Required)
     * 
     */
    @JsonProperty("material")
    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Blade{" +
                "length='" + length + '\'' +
                ", width='" + width + '\'' +
                ", material=" + material +
                '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(length).append(width).append(material).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Blade) == false) {
            return false;
        }
        Blade rhs = ((Blade) other);
        return new EqualsBuilder().append(length, rhs.length).append(width, rhs.width).append(material, rhs.material).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

    public enum Material {

        STEEL("steel"),
        CAST_IRON("cast iron"),
        COPPER("copper");
        private final String value;
        private final static Map<String, Material> CONSTANTS = new HashMap<String, Material>();

        static {
            for (Material c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Material(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Material fromValue(String value) {
            Material constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
