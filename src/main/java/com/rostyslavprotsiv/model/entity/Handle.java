
package com.rostyslavprotsiv.model.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.rostyslavprotsiv.model.exception.HandleLogicalException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * This is a handle of each knife element
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "wooden"
})
public class Handle {

    /**
     * This is a wooden boolean of each knife element
     * (Required)
     * 
     */
    @JsonProperty("wooden")
    @JsonPropertyDescription("This is a wooden boolean of each knife element")
    private Boolean wooden;
    protected HandleMaterial material;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Gets the value of the material property.
     *
     * @return
     *     possible object is
     *     {@link HandleMaterial }
     *
     */
    public HandleMaterial getMaterial() {
        return material;
    }

    /**
     * Sets the value of the material property.
     *
     * @param value
     *     allowed object is
     *     {@link HandleMaterial }
     *
     */
    public void setMaterial(HandleMaterial value)
            throws HandleLogicalException {
        if (wooden) {
            if (value == HandleMaterial.BIRCH
                    || value == HandleMaterial.OAK
                    || value == HandleMaterial.PINE) {
                this.material = value;
            } else {
                throw new HandleLogicalException("Handle should be"
                        + " made with tree!");
            }
        } else {
            if (value == HandleMaterial.IRON
                    || value == HandleMaterial.PLASTIC
                    || value == HandleMaterial.BONE) {
                this.material = value;
            } else {
                throw new HandleLogicalException("Handle should be"
                        + " made with not a tree!");
            }
        }
    }

    /**
     * This is a wooden boolean of each knife element
     * (Required)
     * 
     */
    @JsonProperty("wooden")
    public Boolean isWooden() {
        return wooden;
    }

    /**
     * This is a wooden boolean of each knife element
     * (Required)
     * 
     */
    @JsonProperty("wooden")
    public void setWooden(Boolean value) throws HandleLogicalException {
        if (value) {
            if (material == null
                    || material == HandleMaterial.BIRCH
                    || material == HandleMaterial.OAK
                    || material == HandleMaterial.PINE) {
                this.wooden = value;
            } else {
                throw new HandleLogicalException("Handle should be"
                        + " made with tree!");
            }
        } else {
            if (material == null
                    || material == HandleMaterial.IRON
                    || material == HandleMaterial.PLASTIC
                    || material == HandleMaterial.BONE) {
                this.wooden = value;
            } else {
                throw new HandleLogicalException("Handle should be"
                        + " made with not a tree!");
            }
        }
    }

    @Override
    public String toString() {
        return "Handle{" +
                "wooden=" + wooden +
                ", material=" + material +
                '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(wooden).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Handle) == false) {
            return false;
        }
        Handle rhs = ((Handle) other);
        return new EqualsBuilder().append(wooden, rhs.wooden).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
