
package com.rostyslavprotsiv.model.entity;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * This is a evaluation element of each knife element
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "collectors"
})
public class Value {

    /**
     * This is an element describing whether the knife can be collectable
     * (Required)
     * 
     */
    @JsonProperty("collectors")
    @JsonPropertyDescription("This is an element describing whether the knife can be collectable")
    private Boolean collectors;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * This is an element describing whether the knife can be collectable
     * (Required)
     * 
     */
    @JsonProperty("collectors")
    public Boolean getCollectors() {
        return collectors;
    }

    /**
     * This is an element describing whether the knife can be collectable
     * (Required)
     * 
     */
    @JsonProperty("collectors")
    public void setCollectors(Boolean collectors) {
        this.collectors = collectors;
    }

    @Override
    public String toString() {
        return "Value{" +
                "collectors=" + collectors +
                '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(collectors).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Value) == false) {
            return false;
        }
        Value rhs = ((Value) other);
        return new EqualsBuilder().append(collectors, rhs.collectors).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
