package com.rostyslavprotsiv.model.action.comparator;

import com.rostyslavprotsiv.model.entity.Knife;

import java.util.Comparator;

public class OriginComparator implements Comparator<Knife> {
    @Override
    public int compare(Knife kn, Knife kn1) {
        return kn.getOrigin().compareTo(kn1.getOrigin());//will be from A to X(X is bigger than A)
    }
}
