package com.rostyslavprotsiv.model.action.validator;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONSchemaValidator {

    public void validate(String schemaObject, String jsonObject) {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(JSONSchemaValidator.class
                        .getResourceAsStream(schemaObject)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(JSONSchemaValidator.class
                        .getResourceAsStream(jsonObject)));
        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
        System.out.println("Validation is successful!!!");
    }
}
