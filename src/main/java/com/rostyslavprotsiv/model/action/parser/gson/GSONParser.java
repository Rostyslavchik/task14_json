package com.rostyslavprotsiv.model.action.parser.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rostyslavprotsiv.model.action.parser.gson.deserializer.HandleMaterialDeserializer;
import com.rostyslavprotsiv.model.action.parser.gson.deserializer.HandyDeserializer;
import com.rostyslavprotsiv.model.action.parser.gson.deserializer.MaterialDeserializer;
import com.rostyslavprotsiv.model.action.parser.gson.deserializer.TypeDeserializer;
import com.rostyslavprotsiv.model.entity.Blade;
import com.rostyslavprotsiv.model.entity.ColdSteelSchema;
import com.rostyslavprotsiv.model.entity.HandleMaterial;
import com.rostyslavprotsiv.model.entity.Knife;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class GSONParser {
    public List<Knife> parseFrom(String jsonObject)
            throws FileNotFoundException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Knife.Type.class,
                new TypeDeserializer());
        gsonBuilder.registerTypeAdapter(Knife.Handy.class,
                new HandyDeserializer());
        gsonBuilder.registerTypeAdapter(Blade.Material.class,
                new MaterialDeserializer());
        gsonBuilder.registerTypeAdapter(HandleMaterial.class,
                new HandleMaterialDeserializer());
        Gson gson = gsonBuilder.create();
        ColdSteelSchema coldSteelSchema = gson.fromJson(
                new FileReader(jsonObject), ColdSteelSchema.class);
        return coldSteelSchema.getKnifes();
    }

    public String parseTo(String oldJsonObject)
            throws FileNotFoundException {
        ColdSteelSchema coldSteelSchema = new ColdSteelSchema();
        coldSteelSchema.setKnifes(parseFrom(oldJsonObject));
        Gson gson = new Gson();
        return gson.toJson(coldSteelSchema);
    }
}
