package com.rostyslavprotsiv.model.action.parser.gson.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.rostyslavprotsiv.model.entity.Blade;
import com.rostyslavprotsiv.model.entity.Knife;

import java.lang.reflect.Type;

public class MaterialDeserializer implements JsonDeserializer<Blade.Material> {
    @Override
    public Blade.Material deserialize(JsonElement json, Type typeOfT,
                                      JsonDeserializationContext context)
            throws JsonParseException {
        return Blade.Material.fromValue(json.getAsString());
    }
}
