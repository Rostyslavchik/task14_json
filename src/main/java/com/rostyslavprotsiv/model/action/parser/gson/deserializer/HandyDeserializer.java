package com.rostyslavprotsiv.model.action.parser.gson.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.rostyslavprotsiv.model.entity.Knife;

import java.lang.reflect.Type;

public class HandyDeserializer implements JsonDeserializer<Knife.Handy> {
    @Override
    public Knife.Handy deserialize(JsonElement json, Type typeOfT,
                                   JsonDeserializationContext context)
            throws JsonParseException {
        return Knife.Handy.fromValue(json.getAsString());
    }
}
