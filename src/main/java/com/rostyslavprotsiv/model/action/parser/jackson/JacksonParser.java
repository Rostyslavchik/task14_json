package com.rostyslavprotsiv.model.action.parser.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rostyslavprotsiv.model.action.comparator.OriginComparator;
import com.rostyslavprotsiv.model.entity.ColdSteelSchema;
import com.rostyslavprotsiv.model.entity.Knife;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class JacksonParser {
    public List<Knife> parseFrom(String jsonObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ColdSteelSchema coldSteelSchema = objectMapper.readValue(
                new File(jsonObject), ColdSteelSchema.class);
        return coldSteelSchema.getKnifes();
    }

    public List<Knife> parseFromWithComparator(String jsonObject)
            throws IOException {
        return parseFrom(jsonObject).stream()
                .sorted(new OriginComparator())
                .collect(Collectors.toList());
    }

    public String parseTo(String oldJsonObject) throws IOException {
        ColdSteelSchema coldSteelSchema = new ColdSteelSchema();
        coldSteelSchema.setKnifes(parseFrom(oldJsonObject));
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(coldSteelSchema);
    }
}
