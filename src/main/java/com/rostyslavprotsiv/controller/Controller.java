package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.Main;
import com.rostyslavprotsiv.model.action.parser.gson.GSONParser;
import com.rostyslavprotsiv.model.action.parser.jackson.JacksonParser;
import com.rostyslavprotsiv.model.action.validator.JSONSchemaValidator;
import com.rostyslavprotsiv.model.entity.ColdSteelSchema;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final Menu MENU = new Menu();
    private static final JSONSchemaValidator VALIDATOR
            = new JSONSchemaValidator();
    private static final GSONParser GSON_PARSER = new GSONParser();
    private static final JacksonParser JACKSON_PARSER = new JacksonParser();
    private static final String JSON_PATH = "/json/cold_steel.json";
    private static final String JSON_PATH_FOR_READER
            = ".\\src\\main\\resources\\json\\cold_steel.json";
    private static final String JSON_SCHEMA_PATH = "/json/cold_steel.schema.json";

    public void execute() {
        ColdSteelSchema coldSteelSchema = new ColdSteelSchema();
        MENU.welcome();
        MENU.testValidate(JSON_PATH);
        VALIDATOR.validate(JSON_SCHEMA_PATH, JSON_PATH);
        try {
            coldSteelSchema.setKnifes(
                    GSON_PARSER.parseFrom(JSON_PATH_FOR_READER));
            MENU.testGSONParseFrom(coldSteelSchema.toString());

            coldSteelSchema.setKnifes(JACKSON_PARSER
                    .parseFrom(JSON_PATH_FOR_READER));
            MENU.testJacksonParseFrom(coldSteelSchema.toString());

            coldSteelSchema.setKnifes(JACKSON_PARSER.parseFromWithComparator(
                    JSON_PATH_FOR_READER));
            MENU.testJacksonParseFromWithComparator(coldSteelSchema.toString());

            MENU.testGSONParseTo(GSON_PARSER.parseTo(JSON_PATH_FOR_READER));

            MENU.testJacksonParseTo(JACKSON_PARSER.parseTo(JSON_PATH_FOR_READER));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
