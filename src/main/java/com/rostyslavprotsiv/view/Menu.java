package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Hi, welcome to my code!!");
    }

    public void testValidate(String docName) {
        LOGGER.info("The testing result of validation of" + docName);
    }

    public void testGSONParseFrom(String objects) {
        LOGGER.info("GSON: Parsing from the json file:" + objects);
    }

    public void testJacksonParseFrom(String objects) {
        LOGGER.info("Jackson: Parsing from the json file:" + objects);
    }

    public void testJacksonParseFromWithComparator(String objects) {
        LOGGER.info("Jackson: Parsing with comparator from the json file:"
                + objects);
    }

    public void testGSONParseTo(String jsonObject) {
        LOGGER.info("GSON: Parsing into json:" + jsonObject);
    }

    public void testJacksonParseTo(String jsonObject) {
        LOGGER.info("Jackson: Parsing into json:" + jsonObject);
    }
}
