package com.rostyslavprotsiv.model.action.parser.jackson;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class JacksonParserTest {
    private static JacksonParser parser;
    private static final String RESULT = "{\"knifes\":[{\"type\":\"knife\"," +
            "\"handy\":\"one-arm\",\"origin\":\"China\",\"visual\":{\"blade\"" +
            ":{\"length\":\"15cm\",\"width\":\"13mm\",\"material\":\"steel\"}," +
            "\"handle\":{\"wooden\":true,\"material\":\"oak\"},\"bloodGroove\"" +
            ":true},\"value\":{\"collectors\":true}},{\"type\":\"sword\",\"hand" +
            "y\":\"two-arms\",\"origin\":\"USA\",\"visual\":{\"blade\":{\"length" +
            "\":\"25cm\",\"width\":\"43mm\",\"material\":\"cast iron\"},\"handl" +
            "e\":{\"wooden\":false,\"material\":\"plastic\"},\"bloodGroove\":fa" +
            "lse},\"value\":{\"collectors\":true}},{\"type\":\"axe\",\"handy\":\"" +
            "two-arms\",\"origin\":\"UK\",\"visual\":{\"blade\":{\"length\":\"8" +
            "5cm\",\"width\":\"169mm\",\"material\":\"copper\"},\"handle\":{\"w" +
            "ooden\":true,\"material\":\"birch\"},\"bloodGroove\":true},\"valu" +
            "e\":{\"collectors\":true}},{\"type\":\"dagger\",\"handy\":\"one-a" +
            "rm\",\"origin\":\"Ukraine\",\"visual\":{\"blade\":{\"length\":\"25" +
            "cm\",\"width\":\"16mm\",\"material\":\"steel\"},\"handle\":{\"woo" +
            "den\":false,\"material\":\"iron\"},\"bloodGroove\":false},\"valu" +
            "e\":{\"collectors\":false}},{\"type\":\"knife\",\"handy\":\"two-" +
            "arms\",\"origin\":\"USA\",\"visual\":{\"blade\":{\"length\":\"12" +
            "5cm\",\"width\":\"11mm\",\"material\":\"copper\"},\"handle\":{\"" +
            "wooden\":false,\"material\":\"iron\"},\"bloodGroove\":true},\"val" +
            "ue\":{\"collectors\":false}}]}";
    private static final String JSON_PATH_FOR_READER
            = ".\\src\\main\\resources\\json\\cold_steel.json";

    @BeforeAll
    static void setUp() {
        parser = new JacksonParser();
    }

    @Test
    void testParseTo() {
        try {
            assertEquals(parser.parseTo(JSON_PATH_FOR_READER), RESULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}